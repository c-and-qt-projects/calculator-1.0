This is implementation of calculator desktop application for MacOS.

To compile library, run `make s21_smartcalc.a`.

To run tests run `make test`.

To install application run `make install`.

To start application run `make boot`.

To uninstall application run `make uninstall`.

## Implementation of SmartCalc v1.0

- This version of calculator supports following operations:
    - **Arithmetic operators**:

      | Operator name | Infix notation <br /> (Classic) |
      | --------- | ------ |
      | Brackets | (a + b) |
      | Addition | a + b |
      | Subtraction | a - b |
      | Multiplication | a * b |
      | Division | a / b |
      | Power | a ^ b |
      | Modulus | a % b |
      | Unary plus | +a |
      | Unary minus | -a |

    - **Functions**:
  
      | Function description | Function |
      | ------ | ------ |
      | Computes cosine | cos(x) |
      | Computes sine | sin(x) |
      | Computes tangent | tan(x) |
      | Computes arc cosine | acos(x) |
      | Computes arc sine | asin(x) |
      | Computes arc tangent | atan(x) |
      | Computes square root | sqrt(x) |
      | Computes natural logarithm | ln(x) |
      | Computes common logarithm | log(x) |
