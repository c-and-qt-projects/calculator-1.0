#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <cmath>

extern "C" {
#include "c_part/s21_smart_calc.h"
}

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void digits_numbers();
    void basic_actions();
    void last_del();
    void all_clear();
    void print_func();
    void dec_dot();
    void press_eq();

    void set_xy_graph();
    void graph_build(char* expr_string);
    void on_graph_button_clicked();
};
#endif // MAINWINDOW_H
