#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui -> pushButton_0, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_1, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_2, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_3, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_4, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_5, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_6, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_7, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_8, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_9, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_x, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_e, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_pi, SIGNAL(clicked()), this, SLOT(digits_numbers()));

    connect(ui -> pushButton_plus, SIGNAL(clicked()), this, SLOT(basic_actions()));
    connect(ui -> pushButton_minus, SIGNAL(clicked()), this, SLOT(basic_actions()));
    connect(ui -> pushButton_mult, SIGNAL(clicked()), this, SLOT(basic_actions()));
    connect(ui -> pushButton_div, SIGNAL(clicked()), this, SLOT(basic_actions()));
    connect(ui -> pushButton_mod, SIGNAL(clicked()), this, SLOT(basic_actions()));
    connect(ui -> pushButton_power, SIGNAL(clicked()), this, SLOT(basic_actions()));

    connect(ui -> pushButton_lbrac, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> pushButton_rbrac, SIGNAL(clicked()), this, SLOT(digits_numbers()));

    connect(ui -> pushButton_del, SIGNAL(clicked()), this, SLOT(last_del()));
    connect(ui -> pushButton_AC, SIGNAL(clicked()), this, SLOT(all_clear()));

    connect(ui -> pushButton_dot, SIGNAL(clicked()), this, SLOT(dec_dot()));

    connect(ui -> pushButton_sin, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_cos, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_tan, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_asin, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_acos, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_atan, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_ln, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_log, SIGNAL(clicked()), this, SLOT(print_func()));
    connect(ui -> pushButton_sqrt, SIGNAL(clicked()), this, SLOT(print_func()));

    connect(ui -> pushButton_eq, SIGNAL(clicked()), this, SLOT(press_eq()));
    ui->graphic->xAxis->setRange(-4, 4);
    ui->graphic->yAxis->setRange(0, 9);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow:: digits_numbers()
{
    QPushButton *button = (QPushButton*)sender();
    QString app_string;
    app_string = (ui->app_str->text() + button->text());
    ui->app_str->setText(app_string);
}

void MainWindow:: last_del()
{
    QString app_string = ui->app_str->text();
    QString func_symbols = "sincotanlgqr";
    if (app_string.length() > 0) {
    app_string.chop(1);
    while (app_string.length() > 0 && func_symbols.contains(app_string[app_string.length() - 1], Qt::CaseInsensitive)) {
        app_string.chop(1);
    }
    ui->app_str->setText(app_string);
    }
}

void MainWindow:: all_clear()
{
    ui->app_str->clear();
}

void MainWindow:: print_func()
{
    QPushButton *button = (QPushButton*)sender();
    QString app_string;
    app_string = (ui->app_str->text() + button->text() + '(');
    ui->app_str->setText(app_string);
}

void MainWindow:: basic_actions()
{
    QPushButton *button = (QPushButton*)sender();
    QString app_string = ui->app_str->text();
    QString actions = "+-*/%";
    if (app_string.length() > 0 && actions.contains(app_string[app_string.length() - 1], Qt::CaseInsensitive)) {
        last_del();
    }
    app_string = (ui->app_str->text() + button->text());
    ui->app_str->setText(app_string);
}

void MainWindow:: dec_dot()
{
    QPushButton *button = (QPushButton*)sender();
    QString app_string = ui->app_str->text();
    QString digits = "0123456789";
    int str_index = app_string.length() - 1;
    while (str_index > 0 && digits.contains(app_string.data()[str_index], Qt:: CaseInsensitive)) {
        str_index--;
    }
    if (app_string.data()[str_index] != '.') {
        app_string = (ui->app_str->text() + button -> text());
        ui->app_str->setText(app_string);
    }
}

void MainWindow:: press_eq()
{
    std::string str = ui->app_str->text().toStdString();
    const char *input_string = str.c_str();
    char string_to_pass[256] = "\0";
    strncpy(string_to_pass, input_string, strlen(input_string));
    double result = 0;
    char* to_graph = strstr(string_to_pass, "x\0");
    if (to_graph != NULL) {
        char number_to_insert[30] = "\0";
        sprintf(number_to_insert, "(%.6f)", ui->x_holder->text().toDouble());
        printf("%s\n", number_to_insert);
        replaceWord(input_string, "x",  number_to_insert, string_to_pass);
    }
        int ret = smart_calc(string_to_pass, &result);
        if (ret == 1) {
        ui->result_label->setText("Brackets are not balanced");
    } else if (ret == 2){
        ui->result_label->setText("Brackets placement error");
    } else {
        ui->result_label->setText(QString::number(result, 'g', 15));
    }
}

void MainWindow:: set_xy_graph() {
//  x.clear();
//  y.clear();
  ui->graphic->clearPlottables();
  ui->graphic->clearItems();
  int x_min = ui->label_min_x->text().toInt();
  int y_min = ui->label_min_y->text().toInt();
  int x_max = ui->label_max_x->text().toInt();
  int y_max = ui->label_max_y->text().toInt();
    ui->graphic->xAxis->setRange(x_min, x_max);
    ui->graphic->yAxis->setRange(y_min, y_max);
}

void MainWindow:: graph_build(char* expr_string) {
    double xBegin = ui->label_min_x->text().toInt(), xEnd = ui->label_max_x->text().toInt();
    double smart_result = 0, xCurr = xBegin;
    char string_to_calc[300] = "\0";
    char number_to_insert[30] = "\0";
    double h = xEnd - xBegin;  // размер шага графика
    h /= 10000;
    set_xy_graph();
    QVector<double> x,y;
    int y_min = ui->label_min_y->text().toInt();
    int y_max = ui->label_max_y->text().toInt();
    int number_of_graphs = 0;
    ui->graphic->addGraph();
    while (xCurr < xEnd) {
        strncpy(string_to_calc, expr_string, strlen(expr_string));
        sprintf(number_to_insert, "(%.6f)", xCurr);
        replaceWord(expr_string, "x",  number_to_insert, string_to_calc);
        /*int ret = */smart_calc(string_to_calc, &smart_result);
        if (!(std::isinf(smart_result) || std::isnan(smart_result) || smart_result > y_max || smart_result < y_min)) {
            x.push_back(xCurr);
            y.push_back(smart_result);
        } else if (!x.empty()){
            ui->graphic->graph(number_of_graphs)->setData(x,y);
            x.clear();
            y.clear();
            number_of_graphs++;
            ui->graphic->addGraph();
        }
        xCurr += h;
    }
    ui->graphic->graph(number_of_graphs)->setData(x,y);
    ui->graphic->replot();

}

void MainWindow::on_graph_button_clicked()
{
    std::string str = ui->app_str->text().toStdString();
    const char *input_string = str.c_str();
    char string_to_pass[256] = "\0";
    strncpy(string_to_pass, input_string, strlen(input_string));
    char* to_graph = strstr(string_to_pass, "x\0");
    if (to_graph != NULL) {
        graph_build(string_to_pass);
    }

}

