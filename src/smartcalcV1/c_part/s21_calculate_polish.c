#include "s21_smart_calc.h"

int isNumber(const char *str) {
  return (*str >= '0' && *str <= '9') || *str == 'p' || *str == 'e';
}

void binaryOp(stack_str_double *st, char operation) {
  double op2;
  switch (operation) {
    case '+':
      push_double(st, pop_double(st).number + pop_double(st).number);
      break;
    case '-':
      op2 = pop_double(st).number;
      push_double(st, pop_double(st).number - op2);
      break;
    case '*':
      push_double(st, pop_double(st).number * pop_double(st).number);
      break;
    case '/':
      op2 = pop_double(st).number;
      push_double(st, pop_double(st).number / op2);
      break;
    case '^':
      op2 = pop_double(st).number;
      push_double(st, pow(pop_double(st).number, op2));
      break;
    case '%':
      op2 = pop_double(st).number;
      push_double(st, fmod(pop_double(st).number, op2));
      break;
  }
}

void unaryOp(stack_str_double *st, char operation) {
  switch (operation) {
    case '~':
      push_double(st, -1 * pop_double(st).number);
      break;
    case '!':
      push_double(st, sin(pop_double(st).number));
      break;
    case '?':
      push_double(st, cos(pop_double(st).number));
      break;
    case '#':
      push_double(st, tan(pop_double(st).number));
      break;
    case '$':
      push_double(st, tan(M_PI / 2 - pop_double(st).number));
      break;
    case ':':
      push_double(st, sqrt(pop_double(st).number));
      break;
    case '_':
      push_double(st, log(pop_double(st).number));
      break;
    case '&':
      push_double(st, asin(pop_double(st).number));
      break;
    case '|':
      push_double(st, acos(pop_double(st).number));
      break;
    case '>':
      push_double(st, atan(pop_double(st).number));
      break;
    case '@':
      push_double(st, log10(pop_double(st).number));
      break;
  }
}

void calcExpr(stack_str_double *st, char operation) {
  if (operation == '+' || operation == '-' || operation == '*' ||
      operation == '/' || operation == '^' || operation == '%')
    binaryOp(st, operation);
  else
    unaryOp(st, operation);
}

double getValueRPN(char *main_str) {
  stack_str_double *st = new_stack_double();

  char str[MAX_LEN] = {0};
  strcpy(str, main_str);

  char *tok = strtok(str, " ");
  while (tok != NULL) {
    if (isNumber(tok))
      if (*tok == 'e') {
        push_double(st, M_E);
      } else if (*tok == 'p') {
        push_double(st, M_PI);
      } else {
        push_double(st, atof(tok));
      }
    else
      calcExpr(st, *tok);

    tok = strtok(NULL, " ");
  }
  double ret_value = pop_double(st).number;
  destroy_stack_double(st);
  return ret_value;
}
