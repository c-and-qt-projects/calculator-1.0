#include "s21_smart_calc.h"

int smart_calc(char* string, double* result) {
  int ret = 0;
  // if (input(string) != 0) {
  //     printf("there were some mistakes, programm stopped\n");
  // } else {
  // printf("input infix string : %s\n", string);
  ret = check_for_brackets(string);
  if (ret == 0) {
    replace_func_to_digits(string);
    // printf("after replace infix string : %s\n", string);
    char conv[MAX_LEN] = {0};
    if ((ret = convertedFromInfix(string, conv)) == 0) {
      pic(conv, result);
    }
  }
  // }
  return ret;
}

// int main() {
//     char* expression = calloc(1000, sizeof(char));
//     double result = 0;
//     fgets(expression, 200, stdin);
//     smart_calc(expression, &result);
//     free(expression);
// }
