#include "s21_smart_calc.h"

void replaceWord(const char* s, const char* oldW, const char* newW,
                 char* result) {
  int i, cnt = 0;
  int newWlen = strlen(newW);
  int oldWlen = strlen(oldW);

  for (i = 0; s[i] != '\0'; i++) {
    if (strstr(&s[i], oldW) == &s[i]) {
      cnt++;
      i += oldWlen - 1;
    }
  }
  i = 0;
  while (*s) {
    if (strstr(s, oldW) == s) {
      strcpy(&result[i], newW);
      i += newWlen;
      s += oldWlen;
    } else {
      result[i++] = *s++;
    }
  }
  result[i] = '\0';
}

int check_for_brackets(char* string) {
  int index = 0, ret = 0;
  if (string[index] == ')') {
    ret = 2;
  }
  index++;
  while (string[index] && ret == 0) {
    if (string[index] == ')' && string[index - 1] == '(') {
      ret = 2;
    }
    if (string[index] == '(' && string[index - 1] == ')') {
      ret = 2;
    }
    index++;
  }
  return ret;
}

void replace_func_to_digits(char* string) {
  char* change_from[] = {"asin", "acos", "atan", "sin", "cos", "tan", "ctg",
                         "sqrt", "ln",   "(-",   "log", "π",   "(+"};
  char* change_to[] = {"&", "|", ">",  "!", "?", "#", "$",
                       ":", "_", "(~", "@", "p", "("};
  char dest[MAX_LEN] = {0};
  for (size_t i = 0; i < sizeof(change_from) / sizeof(char*); ++i) {
    replaceWord(string, change_from[i], change_to[i], dest);
    strcpy(string, dest);
  }
  strcpy(string, dest);
}

void pic(char* conv, double* result) {
  // char test[25][80];
  if (strchr(conv, 'x') == NULL) {
    *result = getValueRPN(conv);
    // printf("%lf - is result\n", *result);
  }
  // double x = 0;
  // for (int i = 0; i < 80; i++, x += 4 * M_PI / 79) {
  //     char num[10] = {0};
  //     char val[MAX_LEN] = {0};
  //     snprintf(num, 9, "%lf", x);
  //     replaceWord(conv, "x", num, val);
  //     double y = getValueRPN(val);
  //     int z = y * 12 + 12;
  //     if (z >= 0 && z < 25) {
  //         test[z][i] = 42;
  //     }
  // }
}
