#include "s21_smart_calc.h"

int checkIfOperand(char ch) {
  return ch == 'x' || (ch >= '0' && ch <= '9') || ch == '.' || ch == 'e' ||
         ch == 'p';
}

int precedence(char ch) {
  int ret = -1;
  switch (ch) {
    case '+':
    case '-':
    case '~':
      ret = 1;
      break;
    case '*':
    case '/':
    case '%':
      ret = 2;
      break;
    case '!':
    case '?':
    case '#':
    case '^':
    case '$':
    case ':':
    case '_':
    case '&':
    case '|':
    case '>':
    case '@':
      ret = 3;
      break;
  }

  return ret;
}

int isBalanced(char *expr) {
  int count_bracket = 0;

  int i = 0;
  while (expr[i] && count_bracket >= 0) {
    if (expr[i] == '(') count_bracket++;
    if (expr[i] == ')') count_bracket--;
    i++;
  }
  if (count_bracket != 0) {
    count_bracket = 1;
  }
  return count_bracket;
}

void addToString(char *expr, char *rpn, int *i, int *j) {
  while (checkIfOperand(expr[*i])) {
    rpn[++(*j)] = expr[(*i)++];
  }
  --(*i);
  rpn[++(*j)] = ' ';
}

void moveFromStackToRpn(stack_str_char *st, char *rpn, int *j) {
  rpn[++(*j)] = pop_char(st).opr;
  rpn[++(*j)] = ' ';
}

int covertInfixToPostfix(char *expr, char *rpn) {
  int i, j, err = 0;
  if (isBalanced(expr) != 0) {
    err = 1;
  } else {
    stack_str_char *stack_str_char = new_stack_char();

    if (expr[0] == '-') expr[0] = '~';
    if (expr[0] == '+') expr[0] = ' ';
    for (i = 0, j = -1; expr[i]; ++i) {
      if (checkIfOperand(expr[i])) {
        addToString(expr, rpn, &i, &j);
      } else if (expr[i] == '(') {
        push_char(stack_str_char, expr[i]);
      } else if (expr[i] == ')') {
        while (!isEmpty_char(stack_str_char) &&
               top_char(stack_str_char).opr != '(') {
          moveFromStackToRpn(stack_str_char, rpn, &j);
        }
        pop_char(stack_str_char);
      } else if (expr[i] == ' ') {
        continue;
      } else {
        while (!isEmpty_char(stack_str_char) &&
               precedence(expr[i]) <=
                   precedence(top_char(stack_str_char).opr)) {
          moveFromStackToRpn(stack_str_char, rpn, &j);
        }
        push_char(stack_str_char, expr[i]);
      }
    }

    while (!isEmpty_char(stack_str_char)) {
      moveFromStackToRpn(stack_str_char, rpn, &j);
    }

    destroy_stack_char(stack_str_char);
    rpn[j] = '\0';
  }
  return err;
}

int convertedFromInfix(char *infix_str, char *rpn) {
  return covertInfixToPostfix(infix_str, rpn);
}
