#include "s21_smart_calc.h"

node_str_char* new_node_char(char opr) {
  node_str_char* n = (node_str_char*)malloc(sizeof(node_str_char));
  n->opr = opr;
  n->prev = NULL;
  return n;
}

stack_str_char* new_stack_char() {
  stack_str_char* s = (stack_str_char*)malloc(sizeof(stack_str_char));
  s->first = NULL;
  return s;
}

void push_char(stack_str_char* s, char opr) {
  node_str_char* n = new_node_char(opr);
  n->prev = s->first;
  s->first = n;
}

node_str_char pop_char(stack_str_char* s) {
  node_str_char n = {0, 0};

  if (s->first != NULL) {
    node_str_char* aux;
    aux = s->first;
    s->first = s->first->prev;
    n = *aux;
    free(aux);
  }
  return n;
}

node_str_char top_char(stack_str_char* s) {
  node_str_char n = {0, 0};

  if (s->first != NULL) {
    node_str_char* aux;
    aux = s->first;
    n = *aux;
  }
  return n;
}

void destroy_stack_char(stack_str_char* s) {
  while (s->first != NULL) pop_char(s);
  free(s);
}

int isEmpty_char(stack_str_char* s) { return s->first == NULL; }
