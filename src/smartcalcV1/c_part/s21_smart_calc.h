#ifndef SMART_CALC_H_
#define SMART_CALC_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_LEN 2048

typedef struct node_str_char {
    char opr;
    struct node_str_char* prev;
} node_str_char;

typedef struct stack_str_char {
    node_str_char* first;
} stack_str_char;

typedef struct node_str_double {
    double number;
    struct node_str_double* prev;
} node_str_double;

typedef struct stack_str_double {
    node_str_double* first;
} stack_str_double;

int smart_calc(char* string, double* result);

double getValueRPN(char *str);
int isNumber(const char *str);
void binaryOp(stack_str_double *st, char operation);
void unaryOp(stack_str_double *st, char operation);
void calcExpr(stack_str_double *st, char operation);

int checkIfOperand(char ch);  // infix to postfix
int precedence(char ch);
int isBalanced(char *expr);
void addToString(char *expr, char *rpn, int *i, int *j);
void moveFromStackToRpn(stack_str_char *st, char *rpn, int *j);
int covertInfixToPostfix(char* expr, char *rpn);
int convertedFromInfix(char *infix_str, char *rpn);

node_str_char* new_node_char(char opr);  // char stack
stack_str_char* new_stack_char();
void push_char(stack_str_char* s , char opr);
node_str_char pop_char(stack_str_char* s);
node_str_char top_char(stack_str_char* s);
void destroy_stack_char(stack_str_char* s);
int isEmpty_char(stack_str_char *s);

node_str_double* new_node_double(double num);  // char double
stack_str_double* new_stack_double();
void push_double(stack_str_double* s , double num);
node_str_double pop_double(stack_str_double* s);
void destroy_stack_double(stack_str_double* s);

int input(char *test);  // input
int check(char *a);
int valid(char *a);
int sincheck(const char *a, int *i);
int sqrtcheck(const char *a, int *i);
int coscheck(const char *a, int *i);
int tancheck(const char *a, int *i);
int ctgcheck(const char *a, int *i);
int lncheck(const char *a, int *i);
int switchhub(char *a, int *i);
int check_for_brackets(char* string);

void replaceWord(const char* s, const char* oldW, const char* newW, char *result);
void replace_func_to_digits(char *string);
void pic(char *conv, double* result);

#endif  //  SMART_CALC_H_
