#include "s21_smart_calc.h"

node_str_double* new_node_double(double num) {
  node_str_double* n = (node_str_double*)malloc(sizeof(node_str_double));
  n->number = num;
  n->prev = NULL;
  return n;
}

stack_str_double* new_stack_double() {
  stack_str_double* s = (stack_str_double*)malloc(sizeof(stack_str_double));
  s->first = NULL;
  return s;
}

void push_double(stack_str_double* s, double num) {
  node_str_double* n = new_node_double(num);
  n->prev = s->first;
  s->first = n;
}

node_str_double pop_double(stack_str_double* s) {
  node_str_double n = {0, 0};

  if (s->first != NULL) {
    node_str_double* aux;
    aux = s->first;
    s->first = s->first->prev;
    n = *aux;
    free(aux);
  }
  return n;
}

void destroy_stack_double(stack_str_double* s) {
  while (s->first != NULL) pop_double(s);
  free(s);
}
