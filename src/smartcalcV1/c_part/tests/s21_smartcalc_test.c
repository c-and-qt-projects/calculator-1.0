#include "s21_smartcalc_test.h"

START_TEST(test_binary) {
  double result = 0;
  srand(time(NULL));
  double opper_1 = 0, opper_2 = 0;

  double test_1 = 0;
  char test_1smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    test_1 = opper_1 + opper_2;
    sprintf(test_1smart, "%f   +   %f", opper_1, opper_2);
    smart_calc(test_1smart, &result);
    ck_assert_double_eq_tol(result, test_1, 0.00000001);
  }

  double test_2 = 0;
  char test_2smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    test_2 = opper_1 - opper_2;
    sprintf(test_2smart, "%f - (%f)", opper_1, opper_2);
    smart_calc(test_2smart, &result);
    ck_assert_double_eq_tol(result, test_2, 0.00000001);
  }

  double test_3 = 0;
  char test_3smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    test_3 = -1 * opper_1 - opper_2;
    sprintf(test_3smart, "-(%f) - (%f)", opper_1, opper_2);
    smart_calc(test_3smart, &result);
    ck_assert_double_eq_tol(result, test_3, 0.00000001);
  }

  double test_4 = 0;
  char test_4smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    test_4 = opper_1 * opper_2;
    sprintf(test_4smart, "%f * (%f)", opper_1, opper_2);
    smart_calc(test_4smart, &result);
    ck_assert_double_eq_tol(result, test_4, 0.00000001);
  }

  double test_5 = 0;
  char test_5smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    if (opper_2 != 0) {
      test_5 = opper_1 / opper_2;
      sprintf(test_5smart, "%f / (%f)", opper_1, opper_2);
      smart_calc(test_5smart, &result);
      ck_assert_double_eq_tol(result, test_5, 0.00000001);
    }
  }

  double test_6 = 0;
  char test_6smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 10) + (rand() % 100 / 100.);
    opper_2 = (rand() % 5 + 1) + (rand() % 100 / 100.);
    test_6 = pow(opper_1, opper_2);
    sprintf(test_6smart, "%f ^ %f", opper_1, opper_2);
    smart_calc(test_6smart, &result);
    ck_assert_double_eq_tol(result, test_6, 0.00000001);
  }

  double test_7 = 0;
  char test_7smart[100] = "\0";
  for (int i = 0; i < 200; i++) {
    opper_1 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    opper_2 = (rand() % 1000 - 500) + (rand() % 100 / 100.);
    if (opper_2 != 0) {
      test_7 = fmod(opper_1, opper_2);
      sprintf(test_7smart, "%f %% (%f)", opper_1, opper_2);
      smart_calc(test_7smart, &result);
      ck_assert_double_eq_tol(result, test_7, 0.00000001);
    }
  }

  char test_10smart[20] = "0/0\0";
  smart_calc(test_10smart, &result);
  ck_assert_double_nan(result);

  char test_11smart[20] = "1/0\0";
  smart_calc(test_11smart, &result);
  ck_assert_double_infinite(result);

  char test_12smart[20] = "-1/0\0";
  smart_calc(test_12smart, &result);
  ck_assert_double_infinite(result);
}
END_TEST

START_TEST(test_unary) {
  double result = 0;

  double test_1 = 0;
  char test_1smart[20] = "\0";
  for (double i = -4; i <= 4; i += 0.01) {
    test_1 = sin(i);
    sprintf(test_1smart, "sin(%lf)", i);
    smart_calc(test_1smart, &result);
    ck_assert_double_eq_tol(result, test_1, 0.00000001);
  }

  double test_2 = 0;
  char test_2smart[20] = "\0";
  for (double i = -3; i <= 3; i += 0.01) {
    test_2 = cos(i);
    sprintf(test_2smart, "cos(%lf)", i);
    smart_calc(test_2smart, &result);
    ck_assert_double_eq_tol(result, test_2, 0.00000001);
  }

  double test_3 = 0;
  char test_3smart[20] = "\0";
  for (double i = -1.4; i <= 1.4; i += 0.01) {
    test_3 = tan(i);
    sprintf(test_3smart, "tan(%lf)", i);
    smart_calc(test_3smart, &result);
    ck_assert_double_eq_tol(result, test_3, 0.00000001);
  }

  double test_4 = 0;
  char test_4smart[20] = "\0";
  for (double i = -1; i <= 1; i += 0.01) {
    test_4 = asin(i);
    sprintf(test_4smart, "asin(%lf)", i);
    smart_calc(test_4smart, &result);
    ck_assert_double_eq_tol(result, test_4, 0.00000001);
  }

  double test_5 = 0;
  char test_5smart[20] = "\0";
  for (double i = -1; i <= 1; i += 0.01) {
    test_5 = acos(i);
    sprintf(test_5smart, "acos(%lf)", i);
    smart_calc(test_5smart, &result);
    ck_assert_double_eq_tol(result, test_5, 0.00000001);
  }

  double test_6 = 0;
  char test_6smart[20] = "\0";
  for (double i = -3; i <= 3; i += 0.1) {
    test_6 = atan(i);
    sprintf(test_6smart, "atan(%lf)", i);
    smart_calc(test_6smart, &result);
    ck_assert_double_eq_tol(result, test_6, 0.00000001);
  }

  double test_7 = 0;
  char test_7smart[20] = "\0";
  for (double i = 0.1; i <= 2000; i += 0.1) {
    test_7 = log(i);
    sprintf(test_7smart, "ln(%lf)", i);
    smart_calc(test_7smart, &result);
    ck_assert_double_eq_tol(result, test_7, 0.00000001);
  }

  double test_8 = 0;
  char test_8smart[20] = "\0";
  for (double i = 0.1; i <= 2000; i += 0.1) {
    test_8 = log10(i);
    sprintf(test_8smart, "log(%lf)", i);
    smart_calc(test_8smart, &result);
    ck_assert_double_eq_tol(result, test_8, 0.00000001);
  }

  double test_9 = 0;
  char test_9smart[20] = "\0";
  for (double i = 0; i <= 20; i += 0.1) {
    test_9 = sqrt(i);
    sprintf(test_9smart, "sqrt(%lf)", i);
    smart_calc(test_9smart, &result);
    ck_assert_double_eq_tol(result, test_9, 0.00000001);
  }

  double test_10 = 0;
  char test_10smart[20] = "tan(π/2)\0";
  test_10 = tan(M_PI / 2);
  smart_calc(test_10smart, &result);
  ck_assert_double_eq_tol(result, test_10, 0.0000001);

  char test_11smart[20] = "sqrt(-1)\0";
  smart_calc(test_11smart, &result);
  ck_assert_double_nan(result);

  char test_12smart[20] = "ln(-1)\0";
  smart_calc(test_12smart, &result);
  ck_assert_double_nan(result);
}
END_TEST

START_TEST(test_multiple_operations) {
  double result = 0;
  srand(time(NULL));
  double opper_1 = 0, opper_2 = 0, opper_3 = 0;

  double test_1 = 0;
  char test_1smart[1000] = "\0";
  for (int i = 0; i < 2000; i++) {
    opper_1 = (rand() % 10 - 5) + (rand() % 100 / 100.);
    opper_2 = (rand() % 10 - 5) + (rand() % 100 / 100.);
    opper_3 = (rand() % 10 - 5) + (rand() % 100 / 100.);
    test_1 = sin(opper_1 + 0.34) + cos(opper_2 * (opper_3 - 2));
    sprintf(test_1smart, "sin((%lf) + 0.34) + cos((%lf) * ((%lf) - 2))",
            opper_1, opper_2, opper_3);
    smart_calc(test_1smart, &result);
    ck_assert_double_eq_tol(result, test_1, 0.0000001);
  }

  double test_2 = 0;
  char test_2smart[1000] = "\0";
  for (int i = 0; i < 2000; i++) {
    opper_1 = (rand() % 10 - 5) + (rand() % 100 / 100.);
    opper_2 = (rand() % 10000 / 10000.) * pow(-1, rand() % 2);
    opper_3 = (rand() % 10 - 5) + (rand() % 100 / 100.);
    if (opper_3 != 0) {
      test_2 = fmod((3.54 * (tan(opper_1) + atan(opper_2)) + 21.54), opper_3);
      sprintf(test_2smart, "(3.54 * (tan(%lf) + atan(%lf)) + 21.54) %% (%lf)",
              opper_1, opper_2, opper_3);
      smart_calc(test_2smart, &result);
      ck_assert_double_eq_tol(result, test_2, 0.0000001);
    }
  }

  double test_3 = 0;
  char test_3smart[1000] = "\0";
  for (int i = 0; i < 2000; i++) {
    opper_1 = (rand() % 10000 / 10000.);
    test_3 = asin(acos(opper_1) / (sqrt(25)));
    sprintf(test_3smart, "asin(acos(%lf) / (sqrt(25)))", opper_1);
    smart_calc(test_3smart, &result);
    ck_assert_double_eq_tol(result, test_3, 0.0000001);
  }

  double test_4 = 0;
  char test_4smart[1000] = "\0";
  for (int i = 0; i < 2000; i++) {
    opper_1 = ((rand() % 1000 + 1) / 1000.);
    if (opper_1 < 0) {
      opper_1 *= -1;
    }
    opper_2 = (rand() % 100);
    test_4 = tan(opper_1) + 4.3 * (opper_2 * 2.4 - log10(3.3));
    sprintf(test_4smart, "tan(%lf) + 4.3*(%lf * 2.4 - log(3.3))", opper_1,
            opper_2);
    smart_calc(test_4smart, &result);
    ck_assert_double_eq_tol(result, test_4, 0.0000001);
  }
}
END_TEST

START_TEST(test_errors) {
  double result = 0;

  char test_1_str[1000] = "((2+45/3))";
  int test1 = smart_calc(test_1_str, &result);
  ck_assert_int_eq(test1, 0);

  char test_2_str[1000] = "((2+45/3)";
  int test2 = smart_calc(test_2_str, &result);
  ck_assert_int_eq(test2, 1);

  char test_3_str[1000] = ")(2+45/3))";
  int test3 = smart_calc(test_3_str, &result);
  ck_assert_int_eq(test3, 2);

  char test_4_str[1000] = "2*12()";
  int test4 = smart_calc(test_4_str, &result);
  ck_assert_int_eq(test4, 2);

  char test_5_str[1000] = "2*12)(";
  int test5 = smart_calc(test_5_str, &result);
  ck_assert_int_eq(test5, 2);
}
END_TEST

Suite *s21_string_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("s21_smartcalc");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_binary);
  tcase_add_test(tc_core, test_unary);
  tcase_add_test(tc_core, test_multiple_operations);
  tcase_add_test(tc_core, test_errors);
  suite_add_tcase(s, tc_core);

  return s;
}

int main() {
  int no_failed = 0;
  Suite *s;
  SRunner *runner;
  s = s21_string_suite();
  runner = srunner_create(s);
  srunner_run_all(runner, CK_VERBOSE);
  no_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  no_failed == 0 ? printf("Success\n") : printf("fail\n");
  return 0;
}
